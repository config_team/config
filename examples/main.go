package main

import (
	"fmt"
	"gitlab.srv.pv.km/go-libs/config"
)

type CONSTANT struct {
	Session struct {
		CookieName string `json:"CookieName"`
		Expires    int    `json:"Expires"`
	} `json:"session"`
}

type ENV_STRUCT struct {
	Couchbase interface{} `json:"Couchbase"`
}

func main() {
	const ENV = "env.json"
	const CONST = "constants.json"

	constant := &CONSTANT{}
	env := &ENV_STRUCT{}

	config.ReadFile(CONST, constant)
	config.ReadFile(ENV, env)

	fmt.Println("RESULT CONSTANT.Session.CookieName :: ", constant.Session.CookieName)
	fmt.Println("RESULT CONSTANT.Session.Expires :: ", constant.Session.Expires)
	fmt.Println("RESULT ENV :: ", env)
}
