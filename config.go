package config

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
)

// path to config file by default
var dir *string

func init() {
	dir = flag.String("config", "etc", "path to config folder")
	flag.Parse()
}

// Open file to read
func readConfigFile(filename string) []byte {
	config_str, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(`[FATAL] Config :: readConfigFile error :: '%v'`, err.Error())
	}
	return config_str
}

// Read cofig file in struct
func ReadFile(path string, customStruct interface{}) {
	err := json.Unmarshal(readConfigFile(*dir+`/`+path), &customStruct)
	if err != nil {
		log.Fatal(`[FATAL] Config :: ReadFile error :: '%v'`, err.Error())
	}
}
