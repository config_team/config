package config

import (
	"fmt"
	"testing"
)

type CONST struct {
	Session struct {
		CookieName string `json:"CookieName"`
		Expires    int    `json:"Expires"`
	} `json:"session"`
}

func TestReadFile(t *testing.T) {
	path := `constants.json`
	const_ := &CONST{}
	ReadFile(path, &const_)

	assert(t, const_.Session.CookieName == `session_id`, fmt.Sprintf(`Config parser looked for "session_id", and received %s `, const_.Session.CookieName))
	assert(t, const_.Session.Expires == 50, fmt.Sprintf(`Config parser looked for %s, and received %s `, 50, const_.Session.Expires))
}

func check(t *testing.T, e error) {
	if e != nil {
		t.Error(e)
	}
}

func assert(t *testing.T, condition bool, assertion string) {
	if !condition {
		t.Errorf("Assertion failed: %v", assertion)
	}
}
